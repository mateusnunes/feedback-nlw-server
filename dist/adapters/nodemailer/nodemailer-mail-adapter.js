"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NodemailMailAdapter = void 0;
const nodemailer_1 = __importDefault(require("nodemailer"));
const transport = nodemailer_1.default.createTransport({
    host: "smtp.mailtrap.io",
    port: 2525,
    auth: {
        user: "a226bbdd4da4f8",
        pass: "3c87568bb0af3d"
    }
});
class NodemailMailAdapter {
    async sendMail({ subject, body }) {
        const feedback = await transport.sendMail({
            from: 'Equipe Feedget <ao@feedget.com>',
            to: 'Mateus Nunes <mateusnunes9519@gmail.com>',
            subject,
            html: body,
        });
    }
}
exports.NodemailMailAdapter = NodemailMailAdapter;
