import nodemailer from 'nodemailer'
import { MailAdapter, SendMailData } from "../mail-adapter";

const transport = nodemailer.createTransport({
    host: "smtp.mailtrap.io",
    port: 2525,
    auth: {
        user: "a226bbdd4da4f8",
        pass: "3c87568bb0af3d"
    }
});


export class NodemailMailAdapter implements MailAdapter {
    async sendMail({ subject, body }: SendMailData) {

        const feedback =
            await transport.sendMail({
                from: 'Equipe Feedget <ao@feedget.com>',
                to: 'Mateus Nunes <mateusnunes9519@gmail.com>',
                subject,
                html: body,
            })
    }
}